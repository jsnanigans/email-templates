const fg = require('fast-glob');
const watch = require('./watch');
const vars = require('./variables');
const buildProd = vars.production;
import { registerComponent } from 'mjml-core';
import components from 'mjml-core/lib/components';

function requireUncached(module) {
  delete require.cache[require.resolve(module)];
  return require(module);
}

const fixName = string => {
  return string
    .replace(/([a-z0-9])([A-Z])/g, '$1-$2')
    .replace(/([A-Z])([A-Z])(?=[a-z])/g, '$1-$2')
    .toLowerCase();
};

const loadComponents = rebuildTemplates => {
  const componentFiles = fg.sync(['src/components/*.js']);
  for (const file of componentFiles) {
    registerComponent(require(`../../${file}`));

    if (!buildProd) {
      watch(file, e => {
        const mod = requireUncached(`../../${file}`);
        delete components[fixName(mod.name)];
        registerComponent(mod);

        rebuildTemplates();
      });
    }
  }
};

module.exports = loadComponents;
