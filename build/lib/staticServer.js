module.exports = {
  start: () => {
    var StaticServer = require('static-server');
    var server = new StaticServer({
      rootPath: './static', // required, the root of the server file tree
      port: 3949, // required, the port to listen
      cors: '*' // optional, defaults to undefined
    });

    server.start();
  }
};
