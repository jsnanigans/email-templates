const path = require('path');
const fs = require('fs');
const mjml2html = require('mjml');
const chalk = require('chalk');
const getDistPath = require('./getDistPath');
const vars = require('./variables');
const useBrowserSync = vars.BROWSERSYNC;
const assetHost = vars.ASSET_HOST;

const addBrowserSync = html => {
  if (useBrowserSync === 'true') {
    return html.replace(
      '</body>',
      `<script id="__bs_script__">//<![CDATA[
			document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.7'><\\/script>".replace("HOST", location.hostname));
	//]]></script>
</body>`
    );
  } else {
    return html;
  }
};

const options = {
  fonts: {
    'Brandon Text': [`${assetHost}/styles/font.css`]
  }
};

const compileTemplate = file => {
  let success = false;
  try {
    const { out, name, outName, dir } = getDistPath(file);
    const content = fs.readFileSync(path.resolve(file), 'utf8');

    const start = Date.now();
    const { html, errors } = mjml2html(content, options);
    const time = Date.now() - start;

    if (errors.length) {
      const length = `${chalk.red(errors.length)} error${
        errors.length > 1 ? 's' : ''
      }`;
      console.log(chalk.bold.underline(`\n🛑  build failed with ${length}:\n`));
      let e = 1;
      for (const error of errors) {
        console.log(
          `${e})`,
          `${chalk.red.bold(name)} on line ${chalk.red(error.line)}`
        );
        console.log(`${e})`, `tag name: ${chalk.red(error.tagName)}`);
        console.log(`${e})`, chalk.red(`${error.message}`));
        console.log('');
        e++;
      }
    } else {
      fs.writeFileSync(out, addBrowserSync(html));
      console.log(`✅  compiled ${chalk.blue(name)} to ${dir} (${time}ms)`);
      success = true;
    }
  } catch (e) {
    console.log(e);
  }

  return {
    success
  };
};

module.exports = compileTemplate;
