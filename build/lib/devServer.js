const fs = require('fs');
const path = require('path');
const http = require('http');
const fg = require('fast-glob');
const chalk = require('chalk');
const vars = require('./variables');
const distPath = vars.DIST_FOLDER;

const createServer = port =>
  http.createServer(function(req, res) {
    const files = fg
      .sync([distPath + '/*.html'])
      .map(f => f.replace(new RegExp(`^${distPath}`), ''));
    const status = 200;
    let body = '';

    if (req.url !== '/') {
      for (const file of files) {
        const fullPath = `${distPath}${file}`;

        if (req.url.indexOf(file) !== -1) {
          body = fs.readFileSync(fullPath, 'utf8');
        }
      }
    }

    if (body === '') {
      body = `<ul>
				${files
          .map(f => `<li><a href="http://localhost:${port}${f}">${f}</a></li>`)
          .join('')}
			</ul>`;
    }

    const content_length = body.length;

    res.writeHead(status, {
      'Content-Length': content_length,
      'Content-Type': 'text/html'
    });

    res.end(body);
  });

const server = () => {
  const port = 3939;
  createServer(port).listen(port);
  console.log(
    `Dev Server running: \t${chalk.green.bold(`http://localhost:${port}`)}`
  );
};

module.exports = server;
