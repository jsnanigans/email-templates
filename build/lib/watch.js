const path = require('path');
const fs = require('fs');

const watchFile = (file, action) => {
  const filePath = path.resolve(file);

  let fsWait = false;
  fs.watch(filePath, (event, filename) => {
    if (filename) {
      if (fsWait) return;
      fsWait = setTimeout(() => {
        fsWait = false;
      }, 100);

      action();
    }
  });
};

module.exports = watchFile;
