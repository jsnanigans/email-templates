const path = require('path');
const fs = require('fs');
const vars = require('./variables');

const outDir = path.resolve(vars.DIST_FOLDER);

if (!fs.existsSync(outDir)) {
  fs.mkdirSync(outDir);
}

const getDistPath = file => {
  const name = path.basename(file);
  const outName = name.replace('.mjml', '.html');
  return {
    name,
    outName,
    dir: vars.DIST_FOLDER,
    out: path.resolve(outDir, outName)
  };
};

module.exports = getDistPath;
