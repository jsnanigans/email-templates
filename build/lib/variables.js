const production = !!process.env.PROD;

const env = require('dotenv').config({
  path: production ? '.env.production' : '.env.development'
});

module.exports = { ...env.parsed, production };
