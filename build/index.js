const fs = require('fs');
const path = require('path');
const fg = require('fast-glob');
const watch = require('./lib/watch');
const compileTemplate = require('./lib/compileTemplate');
const server = require('./lib/devServer');
const assetServer = require('./lib/staticServer');
const getDistPath = require('./lib/getDistPath');
const vars = require('./lib/variables');
const distPath = vars.DIST_FOLDER;
const buildProd = vars.production;
const loadComponents = require('./lib/loadComponents');

const options = {};
const outDir = path.resolve(distPath);

(async () => {
  const templates = await fg(['src/templates/*.mjml']);
  const includes = await fg(['src/includes/*.mjml']);

  if (buildProd) {
    loadComponents();
    for (const file of templates) {
      compileTemplate(file);
    }
    return;
  }

  const bs = require('browser-sync').create('My Server');
  bs.init({
    ui: false,
    logSnippet: false,
    ghostMode: true,
    notify: false,
    logLevel: 'silent'
  });

  if (fs.existsSync(outDir)) {
    const distFiles = await fg([`${outDir}/*.html`]);
    for (const file of distFiles) {
      fs.unlinkSync(file);
    }
  }

  loadComponents(() => {
    for (const file of templates) {
      compileTemplate(file);
    }
    bs.reload();
  });

  for (const ic of includes) {
    watch(ic, e => {
      for (const file of templates) {
        const build = compileTemplate(file);
      }
      bs.reload();
    });
  }

  for (const file of templates) {
    compileTemplate(file);
    watch(file, e => {
      const build = compileTemplate(file);
      if (build.success) {
        bs.reload();
      }
    });
  }

  server();
  assetServer.start();
})();
