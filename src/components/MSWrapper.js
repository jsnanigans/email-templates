import MjSection from 'mjml-section';
import { registerDependencies } from 'mjml-validator';
import { suffixCssClasses } from 'mjml-core';

registerDependencies({
  // Tell the validator which tags are allowed as our component's parent
  'mj-body': ['ms-wrapper'],
  'mj-wrapper': ['ms-wrapper'],
  // Tell the validator which tags are allowed as our component's children
  'ms-wrapper': ['mj-section']
});

module.exports = class MSWrapper extends MjSection {
  constructor(props) {
    super(props);
    this.context.containerWidth = '500px';
  }

  renderWrappedChildren() {
    const { children } = this.props;
    const { containerWidth } = this.context;

    return `
      ${this.renderChildren(children, {
        renderer: component =>
          component.constructor.isRawElement()
            ? component.render()
            : `
          <!--[if mso | IE]>
            <tr>
              <td
                ${component.htmlAttributes({
                  align: component.getAttribute('align'),
                  class: suffixCssClasses(
                    component.getAttribute('css-class'),
                    'outlook'
                  ),
                  width: containerWidth
                })}
              >
          <![endif]-->
            ${component.render()}
          <!--[if mso | IE]>
              </td>
            </tr>
          <![endif]-->
        `
      })}
    `;
  }
};
