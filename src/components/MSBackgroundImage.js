const vars = require('../../build/lib/variables');
const host = vars.ASSET_HOST;

import { registerDependencies } from 'mjml-validator';
import { BodyComponent } from 'mjml-core';

registerDependencies({
  // Tell the validator which tags are allowed as our component's parent
  'mj-body': ['ms-background-image'],
  'mj-wrapper': ['ms-background-image'],
  'mj-section': ['ms-background-image'],
  'mj-column': ['ms-background-image'],
  // Tell the validator which tags are allowed as our component's children
  'ms-background-image': ['ms-section']
});

module.exports = class MSBackgroundImage extends BodyComponent {
  static endingTag = true;

  /*
    We could obviously handle all the attributes accepted for Mj Section,
    Column, Image and Text, but let's keep it simple for this example.
  */
  static allowedAttributes = {
    src: 'string',
    repeat: 'enum(no-repeat, repeat)',
    size: 'string'
  };

  static defaultAttributes = {
    src: null,
    repeat: 'no-repeat',
    size: '100%'
  };

  render() {
    const attributes = {
      'background-url': `${host}/assets/${this.getAttribute('src')}`,
      'background-size': this.getAttribute('size'),
      'background-repeat': this.getAttribute('repeat')
    };

    return this.renderMJML(`
    		<mj-section
          ${this.htmlAttributes(attributes)}
          padding="0"
        >
        <mj-column width="100%"> 
            ${this.getContent()}
          </mj-column>
        </mj-section>
    `);
  }
};
