const vars = require('../../build/lib/variables');
const host = vars.ASSET_HOST;

import { registerDependencies } from 'mjml-validator';
import { BodyComponent } from 'mjml-core';

registerDependencies({
  // Tell the validator which tags are allowed as our component's parent
  'mj-hero': ['ms-image'],
  'mj-column': ['ms-image'],
  // Tell the validator which tags are allowed as our component's children
  'ms-image': []
});

module.exports = class MSImage extends BodyComponent {
  static endingTag = true;

  /*
    We could obviously handle all the attributes accepted for Mj Section,
    Column, Image and Text, but let's keep it simple for this example.
  */
  static allowedAttributes = {
    src: 'string',
    align: 'enum(left,center,right)',
    width: 'unit(px,%)',
    padding: 'unit(px){4}',
    href: 'string'
  };

  static defaultAttributes = {
    src: null,
    align: 'left',
    width: 400,
    padding: '0 0 0 0',
    href: null
  };

  render() {
    const attributes = {
      src: `${host}/assets/${this.getAttribute('src')}`,
      width: this.getAttribute('width'),
      align: this.getAttribute('align'),
      href: this.getAttribute('href'),
      padding: this.getAttribute('padding')
    };

    return this.renderMJML(`
        <mj-image
          ${this.htmlAttributes(attributes)}
        >
        </mj-image>
		`);
  }
};
